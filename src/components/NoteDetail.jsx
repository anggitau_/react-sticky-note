const NoteDetail = () => {
    return (
        <div className="note-detail">
            <div className="note-card">
                <span>title</span>
                <span className="text-body">body</span>
                <div className="note-footer">
                    <small>date</small>
                </div>
            </div>
            <br />
        </div>
    );
}

export default NoteDetail;