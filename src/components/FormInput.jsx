import { useState } from "react";

const FormInput = () => {
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [date, setDate] = useState('');
    const charLimit = 300;

    const handleSubmit = (e) => {
        e.preventDefault();
        
        if (title.trim().length > 0 &&
            body.trim().length > 0
        ) {
            const data = {title, body, date};
            console.log(data)
            // add new data here
            setTitle('');
            setBody('');
            setDate('');
        }
    }

    return (
        <>
            <form onSubmit={handleSubmit}>
                <div className="note-card new">
                    <input
                        type="text"
                        placeholder="title"
                        value={title}
                        onChange={(e) => {
                            setTitle(e.target.value);
                        }} />
                    <textarea
                        cols="10"
                        rows="10"
                        placeholder="description"
                        value={body}
                        onChange={(e) => {
                            if (charLimit - e.target.value.length >= 0) {
                                setBody(e.target.value);
                            }
                        }}
                    ></textarea>
                    <input
                        type="date"
                        value={date}
                        onChange={(e) => {
                            setDate(e.target.value);
                        }} />
                    <div className="note-footer">
                        <small>{charLimit - body.length}/300</small>
                        <button className="save" type="submit">
                            Add
                        </button>
                    </div>
                </div>
            </form>
        </>
    );
}

export default FormInput;