const Note = ({ id, title, body, date }) => {
    return (
        <div className="note-card">
            <span>{ title }</span>
            <span className="text-body">{ body }</span>
            <div className="note-footer">
                <small>{ date }</small>
                {/* delete button here */}
            </div>
        </div>
    );
}
 
export default Note;