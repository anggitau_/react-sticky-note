import { FaCircleNotch } from "react-icons/fa";

const Loading = ({ size }) => {
    return (
        <div className="loading">
            <FaCircleNotch className="spin" size={size} />
        </div>
    );
}
 
export default Loading;