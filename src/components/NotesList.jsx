import Note from "./Note";
import { Link } from "react-router-dom";

const NoteList = ({ notes }) => {
    return (
        <div>
            <div className="notes-list">
                {
                    notes.map((note, index) => (
                        <div key={index}>
                            <Link to={`/${note.id}`} className="link">
                                <Note
                                    id={note.id}
                                    title={note.title}
                                    body={note.body}
                                    date={note.date} />
                            </Link>
                        </div>
                    ))
                }
            </div>
        </div>
    );
}
 
export default NoteList;