import Home from "./Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NoteDetail from "./components/NoteDetail";

const App = () => {
    return (
        <Router>
            <div className="container">
                <Switch>
                    <Route exact path={"/"}>
                        <Home />
                    </Route>
                    <Route path={"/:id"}>
                        <NoteDetail />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
 
export default App;