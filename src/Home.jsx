import { useEffect, useState } from "react";
import FormInput from "./components/FormInput";
import Loading from "./components/Loading";
import NotesList from "./components/NotesList";

const Home = () => {
    const [notes, setNotes] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);
    
    useEffect(() => {
        fetch('http://localhost:8090/notes')
        .then(res => {
            if (!res.ok) {
                throw Error('There are something wrong. Please check your code');
            }
            return res.json();
        })
        .then(data => {
            setIsLoading(false);
            setNotes(data);
            setError(null);
        })
        .catch(err => {
            setIsLoading(false);
            setError(err.message);
        });
    }, []);

    return (
        <>
            <h1>my-note</h1>
            <FormInput />
            {
                error ?
                <span>{ error }</span> :
                <></>
            }
            {
                isLoading ?
                <Loading size={'5em'} /> :
                <></>
            }   
            {
                notes ?
                <NotesList notes={notes} /> :
                <></>
            }
        </>
    );
}
 
export default Home;